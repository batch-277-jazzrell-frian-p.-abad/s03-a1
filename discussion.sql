-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);

INSERT INTO artists(name) VALUES ("Rivermaya");

INSERT INTO artists(name) VALUES ("Blackpink");

INSERT INTO artists(name) VALUES ("Taylor Swift");

INSERT INTO artists (name) VALUES (" New Jeans");

INSERT INTO artists (name) VALUES ("Bamboo");

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Born Pink", "2022-09-16", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The album", "2020-10-02", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES("Midnights", "2022-10-21", 3);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("New Jeans", "2022-08-01", 4);

-- Add new album released by Bamboo 
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Light Peace Love", "2005-05-28", 5),
	("As the Music Plays", "2004-01-01", 5);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("fearless", "2008-11-11", 3),
	("Kill This Love", "2019-01-01", 2);

-- insert songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Snow on the Beach", 256, "Pop", 4), 
	("Anti-Hero", 201, "Pop", 4);

INSERT INTO songs(song_name, length, album_id) VALUES ("Bejeweled", 314, 4);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Masaya", 450, "Jazz", 7),
	("Mr. Clay", 357, "Jazz", 7),
	("Noypi", 700, "OPM", 7);

-- add 2 songs per album

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", 528, "OPM", 1),
	("Panahon Na Naman", 601, "OPM", 1);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Yeah Yeah Yeah", 259, "Pop", 2),
	("Hard to Love", 243, "Pop", 2);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Bet You Wanna", 239, "Pop", 3),
	("Ice Cream", 256, "Pop", 3);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Hurt", 258, "K-Pop", 5),
	("Hype Boy", 300, "K-Pop", 5);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Children of the Sun", 333, "Alternative Rock", 6),
	("Truth", 420, "Alternative Rock", 6);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Breath", 424, "Country Pop", 8),
	("Change", 437, "Country Pop", 8);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kill This Love", 314, "Pop", 9),
	("Don't Know What to Do", 322, "Pop", 9);

-- [SECTION] READ data from our database
-- SYNTAX:
	-- SELECT <columns_name> FROM <table_name>;


SELECT * FROM songs;

-- Specify columns that will be shown
SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;



-- To filter the output of the SELECT operation
SELECT * FROM song_name length WHERE genre = "OPM";

SELECT * FROM albums WHERE artist_id = 3;

-- We can use AND and OR keyword for multiple expressions in the WHERE clause

-- Display the tittle and length of the OPM songs that are more than 4 minutes

SELECT song_name, length FROM songs WHERE length > 400 AND genre = "OPM";


SELECT song_name, length, genre FROM songs WHERE genre = "OPM" OR genre = "Jazz";

-- [SECTION] UPDATING RECORDS/DATA
-- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condition>

UPDATE songs SET length = 428 WHERE song_name = "Kundiman";


UPDATE songs SET genre = "Original Pinoy Music" WHERE genre = "OPM";

UPDATE songs SET genre = "Pop" WHERE genre IS NULL;

-- [SECTION] DELETING RECORDS
-- SYNTAX:
	-- DELETE FROM <table_name> WHERE <condition>;
-- Delete all OPM songs that are more than 4 minutes

DELETE FROM songs WHERE genre = "Original Pinoy Music" AND length > 400;

-- Mini Activity 
	-- Delete Pop songs that are greater than 5 minutes

DELETE FROM songs WHERE genre = "Pop" AND length > 500;


DELETE FROM songs WHERE genre = "Pop" AND length = 0;











	